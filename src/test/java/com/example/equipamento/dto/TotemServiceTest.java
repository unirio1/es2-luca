//package com.example.equipamento.dto;
//
//import com.example.equipamento.dto.request.CadastroTotem;
//import com.example.equipamento.exception.TotemNaoEncontradoException;
//import com.example.equipamento.modelo.Totem;
//import com.example.equipamento.service.TotemService;
//
//import org.junit.jupiter.api.Test;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.Assertions.assertNotNull;
//
//public class TotemServiceTest {
//
//    @Test
//    public void testCadastrarTotem() {
//
//        TotemService totemService = new TotemService();
//        Totem totem = new Totem(0,"RJ","teste");
//
//
//        CadastroTotem cadastraTotem = new CadastroTotem(totem);
//
//        assertNotNull(totemService.cadastraTotem(cadastraTotem.getTotem()));
//    }
//    @Test
//    public void testencontrarTotemPorID() {
//
//        TotemService totemService = new TotemService();
//        Totem totem = new Totem(0,"RJ","teste");
//
//
//        CadastroTotem cadastraTotem = new CadastroTotem(totem);
//
//        totemService.cadastraTotem(cadastraTotem.getTotem());
//
//        assertNotNull(TotemNaoEncontradoException.class, () -> String.valueOf(totemService.encontrarTotemPorId(3)));
//    }
//    @Test
//    public void testeAtualizarTotem(){
//
//        TotemService totemService = new TotemService();
//        Totem totem = new Totem(1,"RJ","teste");
//
//
//        CadastroTotem cadastraTotem = new CadastroTotem(totem);
//
//        totemService.cadastraTotem(cadastraTotem.getTotem());
//
//        Totem totemAtualizado  = new Totem(totem.getIdTotem(),"Nova localização", "Nova Descricão");
//
//        totemService.atualizarTotem(1,totemAtualizado);
//
//        Totem resposta = totemService.encontrarTotemPorId(1);
//        assertEquals("Nova localização", resposta.getLocalizacao());
//        assertEquals("Nova Descricão",resposta.getDescricao());
//    }
//}
