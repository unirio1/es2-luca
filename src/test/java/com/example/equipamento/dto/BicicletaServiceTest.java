//package com.example.equipamento.dto;
//
//import com.example.equipamento.dto.request.CadastroBicicleta;
//import com.example.equipamento.exception.BicicletaNaoEncontradaException;
//import com.example.equipamento.modelo.Bicicleta;
//import com.example.equipamento.service.BicicletaService;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//
//import static org.junit.jupiter.api.Assertions.*;
//
//public class BicicletaServiceTest {
//
//    private BicicletaService bicicletaService;
//
//    @BeforeEach
//    public void setUp() {
//        bicicletaService = new BicicletaService();
//    }
//
//    @Test
//    public void testCadastrarBicicleta() {
//
//        Bicicleta bicicleta = new Bicicleta(1, "2015", "caloi", "2015", 2, "Ativa");
//
//        CadastroBicicleta cadastroBicicleta = new CadastroBicicleta(bicicleta);
//
//        assertNotNull(bicicletaService.cadastrarBicicleta(cadastroBicicleta.getBicicleta()));
//    }
//
//    @Test
//    public void testencontrarBicicletaPorID() {
//
//        Bicicleta bicicleta = new Bicicleta(1, "2015", "caloi", "2015", 2, "Ativa");
//
//        CadastroBicicleta cadastroBicicleta = new CadastroBicicleta(bicicleta);
//        bicicletaService.cadastrarBicicleta(cadastroBicicleta.getBicicleta());
//
//        assertNotNull(BicicletaNaoEncontradaException.class, () -> String.valueOf(bicicletaService.encontrarBicicletaPorID(1)));
//
//    }
//
//    @Test
//    public void testeAtualizarBicicleta(){
//
//        Bicicleta bicicleta = new Bicicleta(1, "2015", "caloi", "2015", 2, "Ativa");
//
//        CadastroBicicleta cadastroBicicleta = new CadastroBicicleta(bicicleta);
//        bicicletaService.cadastrarBicicleta(cadastroBicicleta.getBicicleta());
//
//        Bicicleta bicicletaAtualizada  = new Bicicleta(1, "2015", "caloi", "2015", 2, "Desativada");
//
//        bicicletaService.atualizarBicicleta(1,bicicletaAtualizada);
//
//        Bicicleta resposta = bicicletaService.encontrarBicicletaPorID(1);
//        assertEquals("Desativada", resposta.getStatus());
//        assertEquals("2015", resposta.getAno());
//    }
//
//    @Test
//    public void testeAlterarStatusBicicleta(){
//
//        Bicicleta bicicleta = new Bicicleta(1, "2015", "caloi", "2015", 2, "Nova");
//
//        CadastroBicicleta cadastroBicicleta = new CadastroBicicleta(bicicleta);
//        bicicletaService.cadastrarBicicleta(cadastroBicicleta.getBicicleta());
//
//        bicicletaService.alterarStatusBicicleta(1,"Aposentada");
//
//        Bicicleta resposta = bicicletaService.encontrarBicicletaPorID(1);
//        assertEquals("Aposentada", resposta.getStatus());
//        assertEquals("2015", resposta.getAno());
//    }
//
//    @Test
//    public void testeExcluirBicicleta(){
//
//        Bicicleta bicicleta = new Bicicleta(1, "2015", "caloi", "2015", 2, "Ativa");
//
//        CadastroBicicleta cadastroBicicleta = new CadastroBicicleta(bicicleta);
//        bicicletaService.cadastrarBicicleta(cadastroBicicleta.getBicicleta());
//
//        bicicletaService.excluirBicicleta(1);
//
//        assertNotNull(BicicletaNaoEncontradaException.class, () -> String.valueOf(bicicletaService.encontrarBicicletaPorID(1)));
//    }
//}
