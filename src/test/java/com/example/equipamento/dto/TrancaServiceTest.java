//package com.example.equipamento.dto;
//
//
//import com.example.equipamento.dto.request.CadastroTranca;
//import com.example.equipamento.exception.TrancaNaoEncontradaException;
//import com.example.equipamento.modelo.Tranca;
//import com.example.equipamento.service.TrancaService;
//import org.junit.jupiter.api.Test;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.annotation.DirtiesContext;
//
//
//import static org.junit.jupiter.api.Assertions.*;
//@SpringBootTest
//@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
//class TrancaServiceTest {
//
//    @Test
//    public void testCadastrarTranca() {
//
//        TrancaService trancaService = new TrancaService();
//
//        Tranca tranca = new Tranca(1, 5, "centro", 2,"2015","caloi","Active","15/02/2015:12:00");
//
//        CadastroTranca cadastroTranca = new CadastroTranca(tranca);
//
//        assertNotNull(trancaService.cadastrarTranca(cadastroTranca.getTranca()));
//    }
//    @Test
//    public void testencontrarTrancaPorID() {
//
//        TrancaService trancaService = new TrancaService();
//
//
//        Tranca tranca = new Tranca(1, 5, "centro", 2,"2015","caloi","Active","15/02/2015:12:00");
//
//        CadastroTranca cadastroTranca = new CadastroTranca(tranca);
//        trancaService.cadastrarTranca(cadastroTranca.getTranca());
//
//        assertNotNull(TrancaNaoEncontradaException.class, () -> String.valueOf(trancaService.encontrarTrancaPorID(1)));
//
//
//    }
//    @Test
//    public void testeAtualizarTranca(){
//
//        TrancaService trancaService = new TrancaService();
//
//        Tranca tranca = new Tranca(1, 5, "centro", 2,"2015","caloi","Nova","15/02/2015:12:00");
//
//        CadastroTranca cadastroTranca = new CadastroTranca(tranca);
//        trancaService.cadastrarTranca(cadastroTranca.getTranca());
//
//        tranca.setStatus("Livre");
//
//
//        tranca=trancaService.atualizarTranca(1,tranca);
//
//
//        assertEquals("Livre", tranca.getStatus());
//
//
//
//    }
//
//
//
//}