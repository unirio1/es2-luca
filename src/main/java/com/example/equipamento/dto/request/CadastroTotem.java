package com.example.equipamento.dto.request;

import com.example.equipamento.modelo.Totem;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CadastroTotem {

    @NotNull(message = "O atributo 'Totem' não pode ser nulo")
    private Totem Totem;

}
