package com.example.equipamento.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import com.example.equipamento.modelo.Tranca;
import jakarta.validation.constraints.NotNull;



@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CadastroTranca {

    @NotNull(message = "O atributo 'Tranca' não pode ser nulo")
    private Tranca Tranca;
}
