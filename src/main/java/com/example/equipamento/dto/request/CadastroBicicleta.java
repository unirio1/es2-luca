package com.example.equipamento.dto.request;

import com.example.equipamento.modelo.Bicicleta;
import com.example.equipamento.modelo.Tranca;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CadastroBicicleta {
    @NotNull(message = "O atributo 'Bicicleta' não pode ser nulo")
    private Bicicleta Bicicleta;

}
