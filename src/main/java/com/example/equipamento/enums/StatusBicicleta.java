package com.example.equipamento.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum StatusBicicleta {
    DISPONIVEL("Disponível"),
    EM_USO("Em uso"),
    NOVA("Nova"),
    APOSENTADA("Aposentada"),
    REPARO_SOLICITADO("Reparo solicitado"),
    EM_REPARO("Em reparo");

    private final String status;
}
