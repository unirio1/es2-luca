package com.example.equipamento.service;





import com.example.equipamento.exception.*;
import com.example.equipamento.modelo.*;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


import static com.example.equipamento.enums.StatusBicicleta.EM_USO;
import static com.example.equipamento.enums.StatusTranca.*;
import static com.example.equipamento.enums.StatusTranca.EM_REPARO;
import static com.example.equipamento.enums.StatusTranca.NOVA;

@Service
public class TrancaService {
    private final List<Tranca> listaTranca;
    private final List<TrancaTotem> listaTotensComTrancas;
    private int idNovaTranca=0;
    static String caminhoArquivo = "../json/tranca.json";
    static String caminhoArquivoTrancaTotem = "../json/trancaTotem.json";
    public TrancaService() {
        this.listaTotensComTrancas = carregarTotensComTrancas();
        this.listaTranca = carregarTrancas();
        this.encontrarMaiorId();
    }

    public Tranca cadastrarTranca(Tranca tranca){


        tranca.setIdTranca(idNovaTranca);
        tranca.setStatus(NOVA.getStatus());
        listaTranca.add(tranca);
        idNovaTranca++;
        try {
            salvaTranca();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return tranca;
    }
    public List<Tranca> listarTranca() {
        return listaTranca;
    }
    public void salvaTranca() throws IOException {
        try{
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.writeValue(new File(caminhoArquivo),listaTranca);
        }catch (IOException e){
            System.err.println("erro ");
        }

    }
    private void encontrarMaiorId() {
        int maiorId = 0;
        for (Tranca tranca : this.listaTranca) {
            if (tranca.getIdTranca() > maiorId) {
                maiorId = tranca.getIdTranca();
            }
        }
        // Incrementa o maior ID encontrado para obter o próximo ID disponível
        this.idNovaTranca = maiorId+1;
    }
    public Tranca alterarStatusTranca(Integer idTranca, String acao){
        Tranca tranca = encontrarTrancaPorID(idTranca);

        tranca.setStatus(acao);
        try {
            salvaTranca();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return tranca;
    }
    public void incluiTranca(Integer idTranca, Integer numero){
        Tranca tranca = encontrarTrancaPorID(idTranca);

        tranca.setNumero(numero);
        try {
            salvaTranca();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    public void alterarStatusTranca(Tranca tranca, String status){

        tranca.setStatus(status);
        try {
            salvaTranca();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public Tranca trancarTranca(Integer idTranca, Integer bicicleta){

        Tranca tranca = encontrarTrancaPorID(idTranca);

        if(bicicleta!=null){
            Bicicleta bicicletaExistente = new BicicletaService().encontrarBicicletaPorID(bicicleta);
            tranca.setBicicleta(bicicletaExistente.getIdBicicleta());
        }

        tranca.setStatus(OCUPADA.getStatus());
        try {
            salvaTranca();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return tranca;
    }
    public Tranca destrancarTranca(Integer idTranca, Integer bicicleta){

        Tranca tranca = encontrarTrancaPorID(idTranca);

        if(bicicleta!=null){
            Bicicleta bicicletaExistente = new BicicletaService().encontrarBicicletaPorID(bicicleta);
            tranca.setBicicleta(bicicletaExistente.getIdBicicleta());
            bicicletaExistente.setStatus(EM_USO.getStatus());
        }

        tranca.setStatus(LIVRE.getStatus());
        try {
            salvaTranca();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return tranca;
    }

    public static List<Tranca> carregarTrancas() {

        try {
            ObjectMapper objectMapper = new ObjectMapper();
            File arquivo = new File(caminhoArquivo);

            if (!arquivo.exists()) {
                return new ArrayList<>();
            }
            return objectMapper.readValue(arquivo, objectMapper.getTypeFactory().constructCollectionType(List.class, Tranca.class));
        } catch (IOException e) {
            return new ArrayList<>();
        }
    }
    public static List<TrancaTotem> carregarTotensComTrancas() {

        try {
            ObjectMapper objectMapper = new ObjectMapper();
            File arquivo = new File(caminhoArquivoTrancaTotem);

            if (!arquivo.exists()) {
                return new ArrayList<>();
            }
            return objectMapper.readValue(arquivo, objectMapper.getTypeFactory().constructCollectionType(List.class, BicicletaTranca.class));
        } catch (IOException e) {
            return new ArrayList<>();
        }
    }

    public Tranca atualizarTranca(Integer idTranca, Tranca trancaAtualizada) {

        Tranca trancaExistente = encontrarTrancaPorID(idTranca);

        trancaExistente.setNumero(trancaAtualizada.getNumero());
        trancaExistente.setLocalizacao(trancaAtualizada.getLocalizacao());
        trancaExistente.setAnoDeFabricacao(trancaAtualizada.getAnoDeFabricacao());
        trancaExistente.setModelo(trancaAtualizada.getModelo());
        trancaExistente.setStatus(trancaAtualizada.getStatus());
        try {
            salvaTranca();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return trancaExistente;
    }
    public Tranca encontrarTrancaPorID(Integer idTranca) {
        for (Tranca tranca : listaTranca){
            if (tranca.getIdTranca()==(idTranca))
                return tranca;
        }
        throw new TrancaNaoEncontradaException();
    }

    public void excluirTranca(Integer idTranca){
        Tranca trancaExistente = encontrarTrancaPorID(idTranca);
        listaTranca.remove(trancaExistente);
        try {
            salvaTranca();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void salvaTrancaNoTotem() throws IOException {
        String caminhoArquivo = "C:/Users/lucad/OneDrive/Documentos/es2-luca/src/main/java/com/example/equipamento/json/trancaTotem.json";

        try{
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.writeValue(new File(caminhoArquivo),listaTotensComTrancas);
        }catch (IOException e){
            System.err.println("erro ");
        }

    }
    public void retirarTrancaDeTotem(RetirarDaRede retirarDaRede){

        Tranca trancaExistente = new TrancaService().encontrarTrancaPorID(retirarDaRede.getIdTranca());
        Funcionario funcionarioExistente = new Funcionario();
        Totem totemExistente = new TotemService().encontrarTotemPorId(retirarDaRede.getIdTotem());



        totemExistente.setIdTotem(retirarDaRede.getIdTotem());
        funcionarioExistente.setMatricula(retirarDaRede.getIdFuncionario());
        trancaExistente.setIdTranca(retirarDaRede.getIdTranca());

        listaTotensComTrancas.remove(retirarDaRede);

        if(!(trancaExistente.getNumero() >0)){
            throw new TrancaNumeroNaoCadastradoException();
        }

        if(trancaExistente.getStatus().equals(OCUPADA.getStatus())){
            throw new TrancaOcupadaExcpetion();

        }
        this.alterarStatusTranca(trancaExistente, EM_REPARO.getStatus());

        try {
            salvaTrancaNoTotem();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    public void incluiTrancaEmTotem(TrancaTotem trancaTotem){

        Totem totemExistente = new TotemService().encontrarTotemPorId(trancaTotem.getIdTotem());
        Tranca trancaExistente = encontrarTrancaPorID(trancaTotem.getIdTranca());
        Funcionario funcionarioExistente = new Funcionario();

        totemExistente.setIdTotem(trancaTotem.getIdTotem());
        funcionarioExistente.setMatricula(trancaTotem.getIdFuncionario());
        trancaExistente.setIdTranca(trancaTotem.getIdTranca());




        incluiTranca(trancaTotem.getIdTranca(), trancaExistente.getNumero());


        if (!trancaExistente.getStatus().equals(NOVA.getStatus()) && !trancaExistente.getStatus().equals(EM_REPARO.getStatus())){
            throw new TrancaStatusNaoOkIncluirException();
        }
        this.alterarStatusTranca(trancaExistente, LIVRE.getStatus());
        listaTotensComTrancas.add(trancaTotem);
        try {
            salvaTrancaNoTotem();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

   }



}
