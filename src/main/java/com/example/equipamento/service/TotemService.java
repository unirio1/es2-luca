package com.example.equipamento.service;

import com.example.equipamento.exception.TotemNaoEncontradoException;
import com.example.equipamento.modelo.Totem;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class TotemService {
    private final List<Totem> listaTotem;
    private int idNovoTotem = 0;
    static String caminhoArquivo = "../json/totem.json";

    public TotemService() {
        this.listaTotem = carregarTotem();
        this.encontrarMaiorId();
    }
    public Totem cadastraTotem(Totem totem){
        totem.setIdTotem(idNovoTotem);
        listaTotem.add(totem);
        idNovoTotem++;
        try {
            salvaTotem();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return totem;
    }

    public List<Totem> listarTotens() {
        return listaTotem;
    }

    public void salvaTotem() throws IOException {
        try{
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.writeValue(new File(caminhoArquivo),listaTotem);
        }catch (IOException e){
            System.err.println("erro ");
        }

    }

    private void encontrarMaiorId() {
        int maiorId = 0;
        for (Totem Totem : this.listaTotem) {
            if (Totem.getIdTotem() > maiorId) {
                maiorId = Totem.getIdTotem();
            }
        }
        // Incrementa o maior ID encontrado para obter o próximo ID disponível
        this.idNovoTotem = maiorId+1;
    }

    private static List<Totem> carregarTotem() {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            File arquivo = new File(caminhoArquivo);

            if (!arquivo.exists()) {
                return new ArrayList<>();
            }
            return objectMapper.readValue(arquivo, objectMapper.getTypeFactory().constructCollectionType(List.class, Totem.class));
        } catch (IOException e) {
            return new ArrayList<>();
        }
    }

    public Totem atualizarTotem(Integer idTotem, Totem totemAtualizado) {

        Totem totemExistente = encontrarTotemPorId(idTotem);

        totemExistente.setLocalizacao(totemAtualizado.getLocalizacao());
        totemExistente.setDescricao(totemAtualizado.getDescricao());

        try {
            salvaTotem();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return totemExistente;
    }

    public Totem encontrarTotemPorId(Integer idTotem) {
        for (Totem totem : listaTotem){
            if (totem.getIdTotem()==(idTotem))
                return totem;
        }
        throw new TotemNaoEncontradoException();
    }
    public void excluirTotem(Integer idTotem){
        Totem totemExistente = encontrarTotemPorId(idTotem);
        listaTotem.remove(totemExistente);
        try {
            salvaTotem();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
