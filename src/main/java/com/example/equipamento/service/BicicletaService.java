package com.example.equipamento.service;

import com.example.equipamento.controller.FuncionarioAPI;
import com.example.equipamento.exception.*;
import com.example.equipamento.modelo.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.example.equipamento.enums.StatusBicicleta.*;
import static com.example.equipamento.enums.StatusTranca.LIVRE;
import static com.example.equipamento.enums.StatusBicicleta.NOVA;
import static com.example.equipamento.enums.StatusTranca.OCUPADA;


@Service
public class BicicletaService {

    private List<Bicicleta> listaBicicleta;
    private final List<BicicletaTranca> listaTrancasComBicicletas;
    private int idNovaBicicleta=0;
    static String caminhoArquivo = "../json/bicicleta.json";
    static String caminhoArquivoBicicletaTranca = "../json/bicicletaTranca.json";
    static String restauraDados = "../json/bicicletaRestaurarDados.json";
    public BicicletaService() {
        this.listaTrancasComBicicletas = carregarBicicletasNaTranca();
        this.listaBicicleta = carregarBicicletas();
        this.encontrarMaiorId();
    }
    public Bicicleta cadastrarBicicleta(Bicicleta bicicleta){

        bicicleta.setIdBicicleta(idNovaBicicleta);
        bicicleta.setStatus(NOVA.getStatus());
        listaBicicleta.add(bicicleta);
        idNovaBicicleta++;
        try {
            salvaBicicleta();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return bicicleta;
    }
    public List<Bicicleta> listarBicicleta() {
        return listaBicicleta;
    }
    public List<BicicletaTranca> listarBicicletaNaTranca() {
        return listaTrancasComBicicletas;
    }
    public void salvaBicicleta() throws IOException {
        try{
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.writeValue(new File(caminhoArquivo),listaBicicleta);
        }catch (IOException e){
            System.err.println("erro ");
        }

    }
    public List<Bicicleta> restauraDados(){
        this.listaBicicleta = carregaReset();
        return listaBicicleta;
    }
    static List<Bicicleta>carregaReset(){

        try {
            ObjectMapper objectMapper = new ObjectMapper();
            File arquivo = new File(restauraDados);

            if (!arquivo.exists()) {
                return new ArrayList<>();
            }
            return objectMapper.readValue(arquivo, objectMapper.getTypeFactory().constructCollectionType(List.class, Bicicleta.class));
        } catch (IOException e) {
            return new ArrayList<>();
        }
    }



    private void encontrarMaiorId() {
        int maiorId = 0;
        for (Bicicleta bicicleta : this.listaBicicleta) {
            if (bicicleta.getIdBicicleta() > maiorId) {
                maiorId = bicicleta.getIdBicicleta();
            }
        }

        this.idNovaBicicleta = maiorId+1;
    }
    public Bicicleta atualizarBicicleta(int idBicicleta, Bicicleta bicicletaAtualizada) {

        Bicicleta bicicletaExistente = encontrarBicicletaPorID(idBicicleta);

        bicicletaExistente.setMarca(bicicletaAtualizada.getMarca());
        bicicletaExistente.setModelo(bicicletaAtualizada.getModelo());
        bicicletaExistente.setAno(bicicletaAtualizada.getAno());
        bicicletaExistente.setNumero(bicicletaAtualizada.getNumero());
        bicicletaExistente.setStatus(bicicletaAtualizada.getStatus());

        try {
            salvaBicicleta();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return bicicletaExistente;
    }

    public static List<Bicicleta> carregarBicicletas() {

        try {
            ObjectMapper objectMapper = new ObjectMapper();
            File arquivo = new File(caminhoArquivo);

            if (!arquivo.exists()) {
                return new ArrayList<>();
            }
            return objectMapper.readValue(arquivo, objectMapper.getTypeFactory().constructCollectionType(List.class, Bicicleta.class));
        } catch (IOException e) {
            return new ArrayList<>();
        }
    }
    public static List<BicicletaTranca> carregarBicicletasNaTranca() {

        try {
            ObjectMapper objectMapper = new ObjectMapper();
            File arquivo = new File(caminhoArquivoBicicletaTranca);

            if (!arquivo.exists()) {
                return new ArrayList<>();
            }
            return objectMapper.readValue(arquivo, objectMapper.getTypeFactory().constructCollectionType(List.class, BicicletaTranca.class));
        } catch (IOException e) {
            return new ArrayList<>();
        }
    }
    public Bicicleta encontrarBicicletaPorID(int idBicicleta) {
        for (Bicicleta bicicleta : listaBicicleta){
            if (bicicleta.getIdBicicleta()==(idBicicleta))
                return bicicleta;
        }
        throw new BicicletaNaoEncontradaException();
    }
    public void excluirBicicleta(int idBicicleta){
        Bicicleta bicicletaExistente = encontrarBicicletaPorID(idBicicleta);
        listaBicicleta.remove(bicicletaExistente);
        try {
            salvaBicicleta();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void salvaBicicletaNaTranca() throws IOException {


        try{
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.writeValue(new File(caminhoArquivo),listaTrancasComBicicletas);
        }catch (IOException e){
            System.err.println("erro ");
        }

    }

    public void incluiBicicletaEmTranca(BicicletaTranca bicicletaTranca, Funcionario funcionario){

       Tranca trancaExistente = new TrancaService().encontrarTrancaPorID(bicicletaTranca.getIdTranca());
       Bicicleta bicicletaExistente = encontrarBicicletaPorID(bicicletaTranca.getIdBicicleta());


       trancaExistente.setBicicleta(bicicletaTranca.getIdBicicleta());
       trancaExistente.setIdTranca(bicicletaTranca.getIdTranca());

       listaTrancasComBicicletas.add(bicicletaTranca);


        if(!trancaExistente.getStatus().equals(LIVRE.getStatus())){
            throw new TrancaNaoDisponivelException();

        }
        if (!bicicletaExistente.getStatus().equals(NOVA.getStatus()) && !bicicletaExistente.getStatus().equals(EM_REPARO.getStatus())){
            throw new BicicletaStatusNaoOkIncluirException();
        }
        new TrancaService().alterarStatusTranca(bicicletaTranca.getIdTranca(), OCUPADA.getStatus());
        this.alterarStatusBicicleta(bicicletaExistente, DISPONIVEL.getStatus());

        try {
            salvaBicicletaNaTranca();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }
    public void retirarBicicletaDaTranca(RetirarDaRede retirarDaRede){

        Tranca trancaExistente = new TrancaService().encontrarTrancaPorID(retirarDaRede.getIdTranca());
        Funcionario funcionarioExistente = new Funcionario();
        Totem totemExistente = new TotemService().encontrarTotemPorId(retirarDaRede.getIdTotem());

        BicicletaTranca bicicletaExistenteNaTranca = encontrarBicicletaPorIDNaTranca(retirarDaRede.getIdTranca());
        Bicicleta bicicletaExistente = encontrarBicicletaPorID(bicicletaExistenteNaTranca.getIdBicicleta());

        totemExistente.setIdTotem(retirarDaRede.getIdTotem());
        funcionarioExistente.setMatricula(retirarDaRede.getIdFuncionario());
        trancaExistente.setIdTranca(retirarDaRede.getIdTranca());




        if(!trancaExistente.getStatus().equals(OCUPADA.getStatus())){
            throw new TrancaStatusNaoOcupada();

        }
        if (!bicicletaExistente.getStatus().equals(REPARO_SOLICITADO.getStatus())){
            throw new BicicletaStatusNaoOkRetirarException();
        }
        new TrancaService().alterarStatusTranca(retirarDaRede.getIdTranca(), DISPONIVEL.getStatus());
        this.alterarStatusBicicleta(bicicletaExistente, EM_REPARO.getStatus());
        listaTrancasComBicicletas.remove(retirarDaRede);

        try {
            salvaBicicletaNaTranca();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    public Bicicleta alterarStatusBicicleta(Integer idBicicleta, String acao){
        Bicicleta bicicleta = encontrarBicicletaPorID(idBicicleta);


        bicicleta.setStatus(acao);
        try {
            salvaBicicleta();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return bicicleta;
    }
    public void alterarStatusBicicleta(Bicicleta bicicleta, String status){

        bicicleta.setStatus(status);
        try {
            salvaBicicleta();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    public BicicletaTranca buscarBicicletaComIdTranca(Integer idTranca){

        for (BicicletaTranca bicicletaTranca : listaTrancasComBicicletas){
            if (bicicletaTranca.getIdTranca().equals(idTranca))
                return bicicletaTranca;
        }
        throw new TrancaNaoEncontradaException();
    }
    public Bicicleta mostraBicicletaNaTranca(Integer idTranca){

        BicicletaTranca bicicletaTranca = buscarBicicletaComIdTranca(idTranca);
        return encontrarBicicletaPorID(bicicletaTranca.getIdBicicleta());
    }

    public BicicletaTranca encontrarBicicletaPorIDNaTranca(Integer idBicicleta) {
        for (BicicletaTranca bicicletaTranca : listaTrancasComBicicletas){
            if (bicicletaTranca.getIdBicicleta().equals(idBicicleta))
                return bicicletaTranca;
        }
        throw new BicicletaNaoEncontradaException();
    }

}

