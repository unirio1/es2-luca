package com.example.equipamento.controller;


import jakarta.annotation.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import com.example.equipamento.modelo.Funcionario;
import org.springframework.web.bind.annotation.RequestParam;

import java.net.URI;
import java.util.List;

@FeignClient(contextId = "FuncionarioClient", name = "funcionarioService", url = "funcionarioService:https://aluguel-es2daniel.onrender.com/funcionario}")


public interface FuncionarioAPI {


   @GetMapping(value = "/funcionario/{idFuncionario}", produces = "*/*")
    ResponseEntity<Funcionario> encontrarFuncionarioPorId(@PathVariable("idFuncionario") Integer idFuncionario);

}




