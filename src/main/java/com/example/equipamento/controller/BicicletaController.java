package com.example.equipamento.controller;

import com.example.equipamento.exception.ErroException;
import com.example.equipamento.modelo.Bicicleta;


import com.example.equipamento.modelo.Funcionario;
import com.example.equipamento.modelo.RetirarDaRede;
import com.example.equipamento.modelo.BicicletaTranca;
import com.example.equipamento.service.BicicletaService;

import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.FeignClientsConfiguration;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.net.URI;
import java.util.List;


@RestController
@AllArgsConstructor
public class BicicletaController {

    @Autowired
    private FuncionarioAPI funcionarioAPI;


    public Funcionario encontrarFuncionarioPorId(Integer idFuncionario) {
        return funcionarioAPI.encontrarFuncionarioPorId(idFuncionario).getBody();
    }


    @PostMapping("/bicicleta")
    private Bicicleta cadastrarBicicleta(@RequestBody Bicicleta bicicleta) {
        return new BicicletaService().cadastrarBicicleta(bicicleta);
    }
    @GetMapping("/bicicleta")
    private List<Bicicleta> listarBicicletas(){
        return new BicicletaService().listarBicicleta();
    }

    @PutMapping("/bicicleta/{idBicicleta}")
    private Bicicleta atualizarBicicleta(@PathVariable Integer idBicicleta, @Valid @RequestBody Bicicleta bicicletaAtualizada) {
        return new BicicletaService().atualizarBicicleta(idBicicleta, bicicletaAtualizada);
    }
    @DeleteMapping("/bicicleta/{idBicicleta}")
    private void excluirBicicleta(@PathVariable Integer idBicicleta) {
        new BicicletaService().excluirBicicleta(idBicicleta);
    }

    @PostMapping("/bicicleta/integrarNaRede")
    private ResponseEntity<?> incluiBicicletaEmTotem(@Valid@RequestBody BicicletaTranca bicicletaTranca, @RequestParam Integer idFuncionario){

        try {
            Funcionario funcionario = encontrarFuncionarioPorId(idFuncionario);
            new BicicletaService().incluiBicicletaEmTranca(bicicletaTranca, funcionario);
            return ResponseEntity.status(HttpStatus.OK).body("Dados cadastrados");
        }catch (RuntimeException ex){
            ErroException[] erros = new ErroException[1];
            erros[0] = new ErroException("422", ex.getMessage());

            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(erros);
        }
    }

    @PostMapping("/bicicleta/{idBicicleta}/status/{acao}")
    private Bicicleta alterarStatusBicicleta(@Valid@PathVariable Integer idBicicleta, @PathVariable String acao) {
        return new BicicletaService().alterarStatusBicicleta(idBicicleta,acao);
    }
    @PostMapping("/bicicleta/retirarDaRede")
    private ResponseEntity<?> retirarBicicletaDaTranca(@Valid@RequestBody RetirarDaRede retirarDaRede){
        try {
            new BicicletaService().retirarBicicletaDaTranca(retirarDaRede);
            return ResponseEntity.status(HttpStatus.OK).body("Dados cadastrados");
        }catch (RuntimeException ex){
            ErroException[] erros = new ErroException[1];
            erros[0] = new ErroException("422", ex.getMessage());

            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(erros);
        }
    }

    @PostMapping("bicicleta/restaurarDados")
    private List<Bicicleta> restaurarDados(){

        return new BicicletaService().restauraDados();
    }

}


