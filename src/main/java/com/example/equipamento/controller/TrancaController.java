package com.example.equipamento.controller;

import com.example.equipamento.exception.ErroException;
import com.example.equipamento.modelo.*;
import com.example.equipamento.service.BicicletaService;
import com.example.equipamento.service.TrancaService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
public class TrancaController {
    @PostMapping("/tranca")
    private Tranca cadastrarTranca( @RequestBody Tranca tranca) {
        return new TrancaService().cadastrarTranca(tranca);
    }
    @GetMapping("/tranca")
    private List<Tranca> listarTranca(){
        return new TrancaService().listarTranca();
    }

    @PutMapping("/tranca/{idTranca}")
    private Tranca atualizarTranca(@PathVariable int idTranca, @RequestBody Tranca trancatualizada) {
        return new TrancaService().atualizarTranca(idTranca, trancatualizada);
    }
    @DeleteMapping("/tranca/{idTranca}")
    private void excluirTranca(@PathVariable Integer idTranca) {
        new TrancaService().excluirTranca(idTranca);
    }
    @PostMapping("/tranca/integrarNaRede")
    private ResponseEntity<?> incluiBicicletaEmTotem(@Valid @RequestBody TrancaTotem trancaTotem){
        try {
            new TrancaService().incluiTrancaEmTotem(trancaTotem);
            return ResponseEntity.status(HttpStatus.OK).body("Dados cadastrados");
        }catch (RuntimeException ex){
            ErroException[] erros = new ErroException[1];
            erros[0] = new ErroException("422", ex.getMessage());

            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(erros);
        }
    }
    @GetMapping("/tranca/{idTranca}/bicicleta")
    private Bicicleta buscarBicicletaComIdTranca(@PathVariable Integer idTranca){
        return new BicicletaService().mostraBicicletaNaTranca(idTranca);
        }

    @PostMapping("/tranca/retirarDaRede")
    public ResponseEntity<?> retirarDaRede(@Valid @RequestBody RetirarDaRede retirarDaRede){
        try {
            new TrancaService().retirarTrancaDeTotem(retirarDaRede);
            return ResponseEntity.status(HttpStatus.OK).body("Dados cadastrados");
        }catch (RuntimeException ex){
            ErroException[] erros = new ErroException[1];
            erros[0] = new ErroException("422", ex.getMessage());

            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(erros);
        }
    }
    @PostMapping("/tranca/{idTranca}/status/{acao}")
    private Tranca alterarStatusTranca(@Valid@PathVariable Integer idTranca, @PathVariable String acao) {
        return new TrancaService().alterarStatusTranca(idTranca,acao);
    }

    @PostMapping("/tranca/{idTranca}/trancar")
    private Tranca trancarTranca(@Valid@PathVariable Integer idTranca, @RequestBody(required=false) Integer bicicleta ) {
        return new TrancaService().trancarTranca(idTranca,bicicleta);
    }
    @PostMapping("/tranca/{idTranca}/destrancar")
    private Tranca destrancarTranca(@Valid@PathVariable Integer idTranca, @RequestBody(required=false) Integer bicicleta ) {
        return new TrancaService().destrancarTranca(idTranca,bicicleta);
    }


}
