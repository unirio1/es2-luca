package com.example.equipamento.controller;

import com.example.equipamento.modelo.Totem;
import com.example.equipamento.service.TotemService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor

public class TotemController {


    @GetMapping("/totem")
    private List<Totem> listarTotens() {
        return new TotemService().listarTotens();
    }

    @PostMapping("/totem")
    private Totem cadastrarTotem(@RequestBody Totem totem) {
        return new TotemService().cadastraTotem(totem);
    }

    @PutMapping("/totem/{idTotem}")
    private Totem atualizarTotem(@PathVariable int idTotem, @Valid @RequestBody Totem totemAtualizado) {
        return new TotemService().atualizarTotem(idTotem, totemAtualizado);
    }

    @DeleteMapping("/totem/{idTotem}")
    private void excluirTotem(@PathVariable Integer idTotem) {
        new TotemService().excluirTotem(idTotem);
    }
}
//    @GetMapping("/totem/{idTotem}/trancas")
//    private List<Tranca> listarTrancas(){
//        return TotemService.listarTrancas();
//    }
//    @GetMapping("/totem/{idTotem}/bicicletas")
//    private List<Bicicleta> listarBicicletas(){
//        return TotemService.listarBicicletas();
//    }
//}
