package com.example.equipamento.exception;

public class BicicletaNaoEncontradaException extends RuntimeException{
    public BicicletaNaoEncontradaException(){super("Erro: Bicicleta não encontrada");}

}
