package com.example.equipamento.exception;

public class TrancaNumeroNaoCadastradoException extends RuntimeException{
    public TrancaNumeroNaoCadastradoException(){super("Erro: Numero da tranca não cadastrado no sistema");}
}
