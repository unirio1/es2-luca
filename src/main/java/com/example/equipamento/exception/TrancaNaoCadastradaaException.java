package com.example.equipamento.exception;

public class TrancaNaoCadastradaaException extends RuntimeException{
    public TrancaNaoCadastradaaException() {
        super("Erro: Tranca não cadastrada no JSON");
    }
}
