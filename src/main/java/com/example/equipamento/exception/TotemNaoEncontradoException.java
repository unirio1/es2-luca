package com.example.equipamento.exception;

public class TotemNaoEncontradoException extends RuntimeException{
    public TotemNaoEncontradoException() {
        super("Erro: Totem não encontrado");
    }
}
