package com.example.equipamento.exception;

public class BicicletaStatusNaoOkRetirarException extends RuntimeException{
    public BicicletaStatusNaoOkRetirarException(){super("Erro: Status da  bicicleta não é igual a reparo solicitado");}
}
