package com.example.equipamento.exception;

public class TrancaJaCadastradaException extends RuntimeException{
    public TrancaJaCadastradaException() {
        super("Erro: Tranca ja cadastrada");
    }
}
