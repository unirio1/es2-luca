package com.example.equipamento.exception;

public class TrancaNaoEncontradaException extends RuntimeException{
    public TrancaNaoEncontradaException(){super("Erro: Tranca não encontrada");}
}
