package com.example.equipamento.exception;

public class TrancaNaoDisponivelException extends RuntimeException{
    public TrancaNaoDisponivelException(){super("Erro: Tranca não está disponível");}
}
