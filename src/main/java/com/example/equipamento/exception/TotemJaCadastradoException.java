package com.example.equipamento.exception;

public class TotemJaCadastradoException extends RuntimeException{
    public TotemJaCadastradoException() {
        super("Erro: Totem ja cadastrado");
    }
}
