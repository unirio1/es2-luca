package com.example.equipamento.modelo;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Tranca {

    private int idTranca;

    @NotNull(message = "O atributo 'numero' não pode ser nulo")
    private int numero;
    @NotNull(message = "O atributo 'localizacao' não pode ser nulo")
    private String localizacao;
    @NotNull(message = "O atributo 'bicicleta' não pode ser nulo")
    private int bicicleta;
    @NotNull(message = "O atributo 'anoDeFabricacao' não pode ser nulo")
    private String anoDeFabricacao;
    @NotNull(message = "O atributo 'modelo' não pode ser nulo")
    private String modelo;
    @NotNull(message = "O atributo 'status' não pode ser nulo")
    private String status;
    private String dataHora = ("15/02/2015:12:00");

}
