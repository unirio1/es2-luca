package com.example.equipamento.modelo;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TrancaTotem {

    @NotNull(message = "O atributo 'totem' não pode ser nulo")
    private Integer idTotem;
    @NotNull(message = "O atributo 'tranca' não pode ser nulo")
    private  Integer idTranca;
    @NotNull(message = "O atributo 'funcionario' não pode ser nulo")
    private Integer idFuncionario;
    private String statusAcaoReparador ="String";

}
