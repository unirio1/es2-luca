package com.example.equipamento.modelo;


import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Totem {

    private int idTotem;
    @NotNull(message = "O atributo 'localizacao' não pode ser nulo")
    private String localizacao;
    @NotNull(message = "O atributo 'descricao' não pode ser nulo")
    private String descricao;



}
