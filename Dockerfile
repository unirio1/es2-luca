FROM maven:3.9.0-eclipse-temurin-17-alpine AS build
COPY . .
RUN mvn clean package

FROM openjdk:17-jdk-alpine
COPY --from=build /target/Equipamento-0.0.1-SNAPSHOT.jar Equipamento-0.0.1-SNAPSHOT.jar
RUN mkdir json
COPY /src/main/java/com/example/equipamento/json /json


EXPOSE 8080
ENTRYPOINT ["java","-jar","Equipamento-0.0.1-SNAPSHOT.jar"]
